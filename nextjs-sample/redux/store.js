import { createStore, compose } from 'redux';
import rootReducer from './reducers/rootReducer';
const initialState = {
    searchResult: {}
}
const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;
const store = createStore(
    rootReducer(),
    initialState,
    composeEnhancers()
    );

export default store;


