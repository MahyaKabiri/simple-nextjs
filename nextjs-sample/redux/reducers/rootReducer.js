import counterReducer from './searchReducers';
import { combineReducers } from 'redux';

const rootReducer = () =>
    combineReducers({
        searchResult: counterReducer
    });

export default rootReducer;
