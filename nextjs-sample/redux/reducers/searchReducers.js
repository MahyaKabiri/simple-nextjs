import { DECREMENT_COUNTER, INCREMENT_COUNTER } from '../actions/searchActions';
import { SEARCH_RESULTS } from '../actionTypes/searchActionTypes';

const counterReducer = (state = { value: '' }, action) => {
    switch (action.type) {
        case SEARCH_RESULTS:
            return { ...state, value: action.data }
        default:
            return { ...state };
    }
};

export default counterReducer;
