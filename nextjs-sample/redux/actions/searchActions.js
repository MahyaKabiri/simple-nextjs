import { SEARCH_RESULTS } from '../actionTypes/searchActionTypes';

export const getSearchResult = (data) => ({
    type: SEARCH_RESULTS,
    data: data
});