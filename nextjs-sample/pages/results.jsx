import React from "react";
import { connect } from "react-redux";
function Results(props) {
    const openUrl = (url) => window.open(url, "_blank");
    return <> 
    {props.searchResult ? (
        <ul className="results-list">
            {props.searchResult.items.map((i, index) => {
                return (
                    <li key={index}>
                        <a onClick={() => openUrl(i.link)}>{i.title}</a>
                    </li>
                );
            })}
        </ul>
    ) : <div className="not-found">There is no Result</div>}
    </>
}

const mapStateToProps = (state) => ({
    searchResult: state.searchResult.value,
});

const mapDispatchToProps = (dispatch) => {
    return {
        getSearchResult: (data) => dispatch(getSearchResult(data)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Results);