import React from "react";
import { connect } from "react-redux";
import axios from "axios";
import Link from "next/link";
import { Col, Row, Input, Button } from "antd";
const { Search } = Input;
import { getSearchResult } from "../redux/actions/searchActions";
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: "",
      showList: false,
    };
  }

  static async getInitialProps({ store }) {}

  onChangeHandler = (e) => {
    const { value } = e.target;
    this.setState({
      query: value,
    });
    if (!!value) {
      this.fetchSearchResult(value)
        .then((res) => {
          if (res.data.searchInformation.totalResults > 0) {
            this.props.getSearchResult(res.data);
            this.setState({
              showList: !!this.state.query,
            });
          } else {
            this.setState({
              showList: false,
            });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  fetchSearchResult = (query) => {
    return axios.get(
      `https://customsearch.googleapis.com/customsearch/v1?key=AIzaSyDkJF1Ab2-5mLWPdMZpHeWrjaCU2NSjqwE&cx=017576662512468239146:omuauf_lfve&q=${query}`
    );
  };

  onSearch = (value) => {
    this.setState({ query: value });
    history.push("/results");
    console.log(value);
  };

  openUrl = (url) => window.open(url, "_blank");

  render() {
    return (
      <Row className="search-fields">
        <Col xs={12} lg={8} className="search-input">
          <Input
            placeholder="input search text"
            enterButton="Search"
            size="large"
            value={this.state.query}
            onChange={this.onChangeHandler}
          />
          <Button className="search-btn" type="primary">
            <Link href={!!this.state.query ? "/results" : "/"}>
              <a>Search</a>
            </Link>
          </Button>
          {this.state.showList && (
            <ul className="search-result">
              {this.props.searchResult.items.map((i, index) => {
                return (
                  <li key={index}>
                    <a onClick={() => this.openUrl(i.link)}>{i.title}</a>
                  </li>
                );
              })}
            </ul>
          )}
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = (state) => ({
  searchResult: state.searchResult.value,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getSearchResult: (data) => dispatch(getSearchResult(data)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
